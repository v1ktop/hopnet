function [Legal,Ilegal]=Get_Legal_Ilegal_36bit()
    Legal=[];
    Ilegal=[];
    for k=1:1000
        I=int8(rem(floor([0:2^12-1]' * pow2(1-12:0)),2)); %genera todas las combinaciones de 2^12
        G=int8(randi([0 1],size(I,1),36));  %Genera una matriz de 36 bit con valores iguales por columna
        H=[G,I]; %concatena la primera combinacion 
        for i=1:size(I,2)
            H=[H;[I(:,1:i),G,I(:,i+1:end)]]; %intercala el vector de 36bit entre las columnas de la matriz
        end
        j=randi([1,size(H,1)],1,1000); %Selecciona 1000 datos aleatorios para los legales
        Legal=[Legal;H(j,:)]; %
        H(j,:)=[]; % elimina los datos ya seleccionados
        j=randi([1,size(H,1)],1,1000); %Selecciona 1000 datos aleatorios para los ilegales
        Ilegal=[Ilegal;H(j,:)];
    end
end