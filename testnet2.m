
%dim=96;
% Input data for learning
%input  = randi([0 1], 17,96);
%input(input==0)=-1;
A = 0;
load('A.mat', 'A');

SIZE = 10;

[L, IL] = Get_Legals_Ilegals(A, SIZE);

input = L;

%Input data for recognition
%test = randi([0 1], 96,20);

%input=input;
out = train_hpn(input);
W = out.W;
input = out.X;

for i = 1 : SIZE
    P = [input.ID(i,:), input.PW(i,:)];
    P = encoder(P);
    recognise_hp(P,W)
end