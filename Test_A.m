function TIME = Test_A()
    
    A = 0;
    load('A.mat', 'A');
    
    SIZE = 10000;

    [L, IL] = Get_Legals_Ilegals(A, SIZE);
    
    input = [L.ID, L.PW];
    il_input = [IL.ID, IL.PW];
    il_input = encoder(il_input);
    tic;
    
    input = encoder(input);
    out = train(input);
    W_ = out.W;
    TP_in = out.TP;
    TP_out = 0;
    TIME = toc;

    IL_TP = 0;
    
    for i = 1 : SIZE
        if recognise_hp(input(i,:), W_)
            TP_out = TP_out + 1;
        end
    end
    
    for i = 1 : SIZE
        if recognise_hp(il_input(i,:), W_)
            IL_TP = IL_TP + 1;
        end
    end
    
    TP_in
    TP_out
    
    IL_TP
    
    function out = train(X)
        TP = 0;

        Ns = size(X, 2);
        W = zeros(Ns); 
        
        for ii = 1 : size(X, 1)
            P = X(ii, :);
            
            temp = P'*P;

            Wtemp = W+temp;

            Im = eye(length(Wtemp));

            Wptemp = Wtemp-Im;

            if recognise_hp(P, Wptemp)
               TP = TP+1;
            end
            
            W = W+temp;

        end

        Im = eye(length(W))*size(A,1);
        W = W-Im;
        
        out.W = W;
        out.X = X;
        out.TP = TP;
    end
    
end