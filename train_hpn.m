function out = train_hpn(X)

    % A where number of columns = no. of users
    % and no of rows = no. of pattern per user

    % No of nodes
    Ns = size(X.ID,2);

    % intialise weight matrix
    W = zeros(Ns*4);

    % Calculate weight matrix
    ii = 1;
    while ii < size(X.ID,1)

        % Get iith neuron
        A = [X.ID(ii, :), X.PW(ii, :)];

        P = encoder(A);

        % Multiply with transpose
        temp = P'*P;

        % Create a temporal copy of W
        %W = W+temp;
        Wtemp = W+temp;

        % Generate idenitiy matrix
        Im = eye(length(Wtemp))*size(A,1);

        % Substract and contribution matrix
        Wptemp = Wtemp-Im;

        % Test if is a valid pattern
        if recognise_hp(P, Wptemp)
            W = Wptemp;
        else
            X.PW = X.newPW(X,ii);
            ii = ii - 1;
        end
        ii = ii + 1;
        %hola
    end

    % Generate idenitiy matrix
    %Im = eye(length(W))*size(A,1);
    % Substract and contribution matrix
    %W = W-Im;

    out.W = W;
    out.X = X;

end