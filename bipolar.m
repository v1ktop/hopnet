%% Convierte vectores binarios a bipolares
% La función transforma una entrada binaria a una bipolar, lo que significa
% que pasa 1 a +1 y 0 a -1.

function BP = bipolar(A)
    BP = double(A>0);
    BP = 2*BP-1;
end