function [Legal,Ilegal]=Get_Legals_Ilegals(ID,L)
    Legal.ID=ID(1:floor(size(ID,1)/L):size(ID,1),:); %extrae uniformemente separado L usuarios legales
    Ilegal.ID=ID(floor(size(ID,1)/L/2):floor(size(ID,1)/L):size(ID,1),:); %Extre uniformemente separado L usuarios ilegales
    Legal.PW=randi([0 1],size(Legal.ID,1),size(Legal.ID,2)); % se crea los PW se forma aleatoria
    Ilegal.PW=randi([0 1],size(Ilegal.ID,1),size(Ilegal.ID,2)); %se crea los PW aleatoriamente
        
    function PW = genPW(M, i) %Genera PW aleatoriamente
        M.PW(i,:) = randi([0 1],1,size(M.ID,2));
        PW = M.PW;
    end

    Legal.newPW = @(x, y) genPW(x, y);
    Ilegal.newPW = @(x, y) genPW(x, y);

end