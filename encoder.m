%% Se codifica una entrada binaria mediante codificación de Reed-Solomon
% Para agregar esparcimiento a los vectores, la entrada binaria se 
% codifica, dando como salida un vector de 96 elementos. 

function E = encoder(M)
    n = 12;
    k = 6;
    
    E = zeros(size(M,1), size(M,2) * 2);
    
    enc = comm.RSEncoder(n, k);
    enc.BitInput = true;
    
    for i = 1 : size(M, 1)
        msg = M(i,:);
        code = enc(msg');
        E(i,:) = code';
    end
    
    E = bipolar(E);
  
end