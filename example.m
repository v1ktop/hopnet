%% Ejemplo de funcionamiento general de la red de Hopfield.
% En este ejemplo se muestra el funcionamiento general de la red para
% guardar usuarios y contraseñas en la matriza de pesos.

A = 0;                                  % Variable donde se guarda la
load('A.mat', 'A');

SIZE = 10;

[L, ~] = Get_Legals_Ilegals(A, SIZE);

input = L;

out = train_hpn(input);
W = out.W;
input = out.X;
TP = 0;
for i = 1 : SIZE
    P = [input.ID(i,:), input.PW(i,:)];
    P = encoder(P);
    if recognise_hp(P,W)
        TP = TP + 1;
    end
end

TP