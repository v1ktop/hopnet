function Test_B()    

    SIZE=1000000;
    
    [L, Il] = Get_Legal_Ilegal_36bit();
    %Entrenamiento de la HNN con los datos legales
    [out,tp] = train(L);
    disp("cantidad de verdaderos positivos")
    tp
    W = out.W;
    input = encoder(L);
    tp=0;
 	%comprobar la capacidad de recuerdo de la red
    for i = 1 : size(input,1)
        P = input(i,:);
        
        if(recognise_hp(P,W))
            tp=tp+1;
        end
    end
    
    %comprobar la capacidad de rechazo
    fp=0
    input = encoder(Il);
    for i = 1 : size(input,1)
        P = input(i,:);
        
        if(recognise_hp(P,W))
            fp=fp+1;
        end
    end
    disp("Catidad de falsos positivos")
    fp
    
    %funcion de entrenamiento
    function [out,TP]=train(X)
        % A where number of columns = no. of users
        % and no of rows = no. of pattern per user
        TP=0;
        % No of nodes
        Ns = size(X,2);
        
        % intialise weight matrix
        W = zeros(Ns*2);
        X = encoder(X);
        % Calculate weight matrix
        ii = 1;
        while ii < size(X,1)

            % Get iith neuron
            P = X(ii, :);

            % Multiply with transpose
            temp = P'*P;

            % Create a temporal copy of W
            %W = W+temp;
            Wtemp = W+temp;

            % Generate idenitiy matrix
            Im = eye(length(Wtemp));

            % Substract and contribution matrix
            Wptemp = Wtemp-Im;
            W = Wptemp;
            % Test if is a valid pattern
            if recognise_hp(P, Wptemp)
               TP=TP+1;
            end
            ii = ii + 1;
        end

        out.W = W;
        out.X = X;

    end

end