function out = recognise_hp(pattern,W)

    % Multiply
    R = W*pattern';

    % Make bipolar
    BP = bipolar(R);

    out = isequal(pattern',BP);
end